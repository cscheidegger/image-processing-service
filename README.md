# AeTrapp Image Processing Service

This project is part of [AeTrapp](http://www.aetrapp.org/) - a collaborative effort designed for community engagement in monitoring population of *Aedes aegypti* mosquitoes, which is a known vector of several diseases including dengue, Zika, chikungunya and urban yellow fever. The idea behind this initiative is to provide an easy-to-use mobile app as a tool for getting instructions to create your own traps, capture samples containing eggs of *Aedes*, share them to the community and to acquire informations about its incidence in a region shared by other users.

<img src="assets/example_1.png" alt="Original sample (left), sample after recognition (right)" />
<h6 align=center>Original sample (left) and after recognition process (right)</h6>

Check our [contributing guide](CONTRIBUTING.md) and feel free to open issues. 

### How it works?

This repository contains source code for image processing tasks applied to automatic detection and counting of eggs of *Aedes aegypti*. The activity chart below shows a summary of its operation.

![Dalek Sequence](https://yuml.me/cd182870.png)

- Pre-Processing routines are responsible for checking the quality of an input image such its positioning, blur rate, shadow overlay, etc.
- Segmentation routines driscriminate relevant objects (eggs or cluster of eggs) from the background.
- Eggs and clusters are currently recognized by their size, shape and color.

### Download the Image Database
Link for download: https://drive.google.com/open?id=15YtFv4kor01GsTeiZGsP3Wi8au78udQm

### More from AeTrapp project
- Mobile: https://gitlab.com/aetrapp/mobile.
- WebApp: https://gitlab.com/aetrapp/dashboard.
- API: https://gitlab.com/aetrapp/api.

## Installing

Node.js 8+ and Docker are required. Clone this repository locally and download the model:

```
    git clone <repository_url>
    cd <repository_path>
    ./download-model.sh
```

Build and start the server with Docker:

    docker-compose up

Then, access the dashboard at http://localhost:3131/agenda. To create your first job, make a POST request to `/agenda/api/jobs/create` like in the following example. You can change "imageUrl" value to any image available:

````shell
curl -H "Content-Type: application/json" -X POST -d \
    '{
      "jobName": "process image",
      "jobSchedule": "now",
      "jobData": {
         "image": {
             "url": "https://github.com/aetrapp/image-processing-service/raw/master/samples/06.4SEM.CENC.INTRA.SONY.jpg"
         }
      }
     }' http://localhost:3131/agenda/api/jobs/create
````

If you need shell access to the server, get your container name with `docker ps` and run:

    docker exec -i -t <imageprocessingservice_container_name> /bin/bash

### Deploying new version to production

AeTrapp's main API server checks every five minutes the current version of the Image Processing Server, and will perform re-analysis of samples if a new version is found. To start this process:

- Change algorithm's code and commit;
- Change `ipsVersion` value in `package.json` and commit:

```
{
  "name": "image-processing-service",
  "version": "0.0.1",
  "ipsVersion": "1.4.0", <- CHANGE THIS LINE
  "description": "Aetrapp Image Processing Service",
...
```

- Pull changes to production.

This will start automatically the processes of sample re-analysis by the API server.

# Changelog

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [1.4.1] - 2018-06-12

### Added
- Local blur detection.
- Remove image background.

### Changed
- CNN model path changed from static to dynamic.

## Contributors
Special thanks to:

<!-- ALL-CONTRIBUTORS-LIST:START - Do not remove or modify this section -->
| [<img src="https://avatars.githubusercontent.com/joaoherrera" width="100px;"/><br /><sub>João Herrera</sub>](http://https://github.com/joaoherrera)<br /> [💬](#question-joaoherrera "Answering Questions") [👀](#review-joaoherrera "Reviewed Pull Requests") [📖](https://github.com/aetrapp/image-processing-service/commits?author=joaoherrera "Documentation") [💻](https://github.com/aetrapp/image-processing-service/commits?author=joaoherrera "Code") | [<img src="https://avatars.githubusercontent.com/vgeorge" width="100px;"/><br /><sub>Vitor George</sub>](http://aparabolica.com.br)<br /> [💬](#question-vgeorge "Answering Questions") [👀](#review-vgeorge "Reviewed Pull Requests") [📖](https://github.com/aetrapp/image-processing-service/commits?author=vgeorge "Documentation") [💻](https://github.com/aetrapp/image-processing-service/commits?author=vgeorge "Code") | [<img src="https://avatars.githubusercontent.com/miguelpeixe" width="100px;"/><br /><sub>Miguel Peixe</sub>](http://aparabolica.com.br)<br /> [💻](https://github.com/aetrapp/image-processing-service/commits?author=miguelpeixe "Code") | [<img src="https://avatars.githubusercontent.com/cscheidegger" width="100px;"/><br /><sub>Caio Sheidegger</sub>](https://github.com/cscheidegger)<br /> [🐛](https://github.com/aetrapp/image-processing-service/issues?author=cscheidegger "Bug reports") | [<img src="https://avatars.githubusercontent.com/odascatolini" width="100px;"/><br /><sub>Oda Scatolini</sub>](https://github.com/odascatolini)<br /> [🤔]("" "Ideas & Planning") [📋](https://www.aetrapp.org/ "Event Organizers") [📖](https://github.com/aetrapp/image-processing-service/commits?author=odascatolini "Documentation") [🐛](https://github.com/aetrapp/image-processing-service/issues?author=odascatolini "Bug reports") |
| :---: | :---: | :---: | :---: | :---: |
<!-- ALL-CONTRIBUTORS-LIST:END -->

This project follows the [all-contributors](https://github.com/kentcdodds/all-contributors) specification.

## License

[GPL-3.0](LICENSE)

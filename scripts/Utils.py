# -*- coding: utf-8 -*-

import numpy as np
import cv2
import IO
from scipy import ndimage
from skimage import measure, feature, color, draw, morphology

# Crop image acording to the center and radius of palette
# image: rgb image
# circle_params: circle params
def crop_image(image, circle_params):
	rows, cols = image.shape[:2]

	center_x = circle_params[0]
	center_y = circle_params[1]
	radius = circle_params[2]

	c = 1.5 # constant determining how long is the superior/inferior area to be croped.
	b = ((radius * 54) / 12) / 2 # left/right border factor

	if cols > rows:
		image = ndimage.rotate(image, 90)
		center_x, center_y = center_y, center_x
		rows, cols = cols, rows


	x = int(center_x - c * radius)
	y = int(center_y - b)
	width = int(2 * c * radius)
	height = int(2 * b)


	if x < 0:
		x = 0

	if y < 0:
		y = 0

	if width > cols:
		width = cols - 1

	if height > rows:
		height = rows - 1

	#print x, y, width, height
	roi = image[x : x + width, y : y + height]

	return roi


 
# During a few tests, some of the circles weren't been correctfully identified.
# Coincidentlly, these images have high resolutions. After decreasing it, the problem was solved.
# This method adjust the values of an image to a workable resolution.
# im: 8 or 32 bits image
# Return scaled image
def adjust_resolution(im):
	rows, cols, _ = im.shape

	if rows < 2000 and cols < 1000:
		print(IO.json_packing_error('ERR_008'))
		exit()

	return im



# Even this program accepting images both in portrait or landscape orientations
# I decided to fix the portrait as the orientation pattern.
# im: 8 or 32 bits image
# Returns transladed image.
def adjust_position(im):
	rows, cols, _ = im.shape

	if cols > rows:
		im = cv2.transpose(im)
		im = cv2.flip(im, 1)

	return im



# Increase image contrast using CLAHE adaptive contrast on every channel
# imrgb: RGB image
def adjust_contrast(imrgb):
	clahe = cv2.createCLAHE(clipLimit=1.0, tileGridSize=(2,2))

	imrgb[:,:,0] = clahe.apply(imrgb[:,:,0])
	imrgb[:,:,1] = clahe.apply(imrgb[:,:,1])
	imrgb[:,:,2] = clahe.apply(imrgb[:,:,2])

	return imrgb



# Devide an image in 4 quadrants 'lv' times
# im: RGB image
# lv times to be reduced
def crop_in_quadrants(im, lv):
	rows, cols = im.shape[:2]
	
	lenXY = pow(2, lv)	
	lenX = rows // lenXY		
	lenY = cols // lenXY

	pieces = []
	cline = 0
		
	for line in range(lenXY):
		row = im[cline:cline + lenX + 1:1, ::]
		
		ccol = 0
		for col in range(lenXY):
			pieces.append(row[::, ccol:ccol + lenY + 1:1])
			ccol += lenY

		cline += lenX
	
	return pieces



# This method aims to solve the problems of shadows on the border
# and must be called before the crop by the circular measure
# imrgb: RGB image
def remove_background(imrgb):
	rows, cols, _ = imrgb.shape

	# The idea here is to use RANSAC for line recognition.
	# Then 'cut' them to remove possible shadows and dirty
	mask = np.ones((rows, cols))
	mask[:, cols//3:] = 0

	# we gonna find two lines (left and right)
	for i in range(2):
		edges = feature.canny(color.rgb2gray(imrgb), sigma=1, mask=mask.astype(bool))
		edge_pts = np.array(np.nonzero(edges), dtype=float).T
		edge_pts_xy = edge_pts[:, ::-1]

		# apply ransac
		model_robust, _ = measure.ransac(edge_pts_xy, measure.LineModelND, min_samples=2, residual_threshold=2, max_trials=1000)
		x = np.arange(rows)
		y = model_robust.predict_y(x)

		# concatenating first and last points
		x = x[y >= 0]
		y = y[y >= 0]
		x = x[y <= cols]
		y = y[y <= rows]
	
		if len(x) > 0 and len(y) > 0:

			# define each initial and final line pixels
			startx = x[0] if (cols - x[0]) < x[0] else x[-1]
			starty = 0 if (rows - y[0]) > y[0] else rows - 1
			endx = x[-1] if startx == x[0] else x[0]
			endy = rows - 1 if starty == 0 else 0
			
			rr, cc = draw.polygon([starty, starty, endy, endy], [0, endx, startx, 0])
			edges[:, :cols//2] = 0
			
			imrgb[rr, cc] = (255, 255, 255)


		for i in range(2):
			imrgb = cv2.transpose(imrgb)
			imrgb = cv2.flip(imrgb, 1)

	return imrgb
